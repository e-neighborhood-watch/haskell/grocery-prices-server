{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Lib (initializeApp)
import qualified Data.Sequence as Seq
import Control.Concurrent.STM
import Test.Hspec
import Test.Hspec.Wai

main :: IO ()
main = hspec spec

spec :: Spec
spec = with (newTQueueIO >>= initializeApp Seq.empty) $ do
  describe "GET /entries" $ do
    it "responds with 200" $ do
      get "/entries" `shouldRespondWith` 200
