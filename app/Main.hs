module Main (main) where

import Lib

main :: IO ()
main =
  startApp "grocery-prices.csv"
