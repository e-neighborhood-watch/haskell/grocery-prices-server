{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeOperators     #-}
module Lib
    ( startApp
    , initializeApp
    , app
    ) where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Reader
import Data.Aeson
import Data.Aeson.TH
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL
import qualified Data.Csv as Csv
import Data.Csv (FromNamedRecord,ToNamedRecord,DefaultOrdered(..),ToField(..),FromField(..))
import Data.Foldable
import Data.Maybe
import qualified Data.Sequence as Seq
import Data.Sequence (Seq, (|>))
import qualified Data.Set as Set
import Data.Set (Set)
import Data.Text (Text)
import Data.Time.Calendar.OrdinalDate
import qualified Data.Vector as Vector
import Data.Vector (Vector)
import GHC.Generics
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import System.IO
import Text.Read (readEither)

data State =
  State
    { csvWriterQueue :: TQueue (Vector Product)
    , productList :: TVar (Seq (Vector Product))
    }

type AppM = ReaderT State Handler

newtype MyDay =
  MyDay
    { getDay :: Day
    }
  deriving
    ( Eq
    )

instance Show MyDay where
  show =
    show . getDay

newtype SetText =
  SetText
    { getSetText :: Set Text
    }
  deriving
    ( Eq
    )

instance Show SetText where
  show =
    show . getSetText

data Unit
  = Liters
  | Kilograms
  | Count
  deriving
    ( Eq
    , Show
    )

instance FromField Unit where
  parseField s
    | s == "L" =
      pure Liters
    | s == "kg" =
      pure Kilograms
    | s == "count" =
      pure Count
    | otherwise =
      fail "unrecognized unit"

instance ToField Unit where
  toField Liters =
    "L"
  toField Kilograms =
    "kg"
  toField Count =
    "count"

data Product =
  Product
    { category :: Text
    , variety :: Text
    , specifics :: Text
    , price :: Int
    , amount :: Float
    , unit :: Unit
    , tags :: SetText
    , notes :: Text
    , purchaseDate :: MyDay
    }
    deriving
      ( Eq
      , Show
      , Generic
      )

instance ToField MyDay where
  toField =
    toField . show . getDay

instance FromField MyDay where
  parseField field =
    do
      str <- parseField field
      case
        readEither str
       of
        Left err ->
          fail err

        Right day ->
          pure $ MyDay day

instance ToField SetText where
  toField =
    toField . show . Set.toList . getSetText

instance FromField SetText where
  parseField field =
    do
      str <- parseField field
      case
        readEither str
       of
        Left err ->
          fail err

        Right ls ->
          pure $ SetText $ Set.fromList ls

instance FromNamedRecord Product
instance ToNamedRecord Product
instance DefaultOrdered Product where
  headerOrder _ =
    Csv.header
      [ "category"
      , "variety"
      , "specifics"
      , "price"
      , "amount"
      , "unit"
      , "tags"
      , "notes"
      , "purchaseDate"
      ]

data ProductsResponse =
  ProductsResponse
    { nextIndex :: Int
    , products :: Vector Product
    }
    deriving
      ( Eq
      , Show
      )


makeProductsResponse :: Int -> Maybe Int -> Seq (Vector Product) -> ProductsResponse
makeProductsResponse newProducts numSeen storedProducts =
  ProductsResponse
    { nextIndex =
      Seq.length storedProducts + newProducts
    , products =
      fold $ Seq.drop (fromMaybe 0 numSeen) storedProducts
    }

$(deriveJSON defaultOptions{ unwrapUnaryRecords = True } ''MyDay)
$(deriveJSON defaultOptions{ unwrapUnaryRecords = True } ''SetText)
$(deriveJSON defaultOptions ''Unit)
$(deriveJSON defaultOptions ''Product)
$(deriveJSON defaultOptions ''ProductsResponse)

type API =
  "entries" :> QueryParam "after" Int :> Get '[JSON] ProductsResponse
  :<|> "entries" :> QueryParam "after" Int :> ReqBody '[JSON] (Vector Product) :> Post '[JSON] ProductsResponse

writeToCsv :: Handle -> TQueue (Vector Product) -> IO ()
writeToCsv csvHandle toWrite =
  do
    newProducts <- atomically $ peekTQueue toWrite >> flushTQueue toWrite
    let
      newRecords =
        Csv.encodeDefaultOrderedByNameWith
          Csv.defaultEncodeOptions { Csv.encIncludeHeader = False }
          (newProducts >>= Vector.toList)
    BL.hPut csvHandle newRecords

startApp :: FilePath -> IO ()
startApp csvFile =
  do
    fileContents <- BS.readFile csvFile
    initialProducts <-
      if
        BS.null fileContents
      then
        do
          BS.writeFile csvFile $ BL.toStrict $ Csv.encodeDefaultOrderedByName ([] :: [Product])
          pure Vector.empty
      else
        case
          Csv.decodeByName $ BL.fromStrict fileContents
        of
          Left err ->
            fail err

          Right (_, readProducts) ->
            pure readProducts
    csv <- openFile csvFile AppendMode
    csvQueue <- newTQueueIO
    _ <- forkIO $ forever $ writeToCsv csv csvQueue
    initializeApp (if Vector.null initialProducts then Seq.empty else Seq.singleton initialProducts) csvQueue >>= run 8080

initializeApp :: Seq (Vector Product) -> TQueue (Vector Product) -> IO Application
initializeApp initialProducts csvQueue =
  fmap app $ State csvQueue <$> newTVarIO initialProducts

app :: State -> Application
app s =
  serve api $ hoistServer api (flip runReaderT s) server

api :: Proxy API
api = Proxy

server :: ServerT API AppM
server =
  getEntries :<|> postEntries
  where
    getEntries :: Maybe Int -> AppM ProductsResponse
    getEntries n =
      ask >>= liftIO . atomically . fmap (makeProductsResponse 0 n) . readTVar . productList

    postEntries :: Maybe Int -> Vector Product -> AppM ProductsResponse
    postEntries n newProducts =
      if
        Vector.null newProducts
      then
        getEntries n
      else
        do
          State{productList = pl, csvWriterQueue = csvQueue} <- ask
          liftIO $ atomically $ writeTQueue csvQueue newProducts
          fmap (makeProductsResponse 1 n) $ liftIO $ atomically $
            do
              oldProducts <- readTVar pl
              writeTVar pl $ oldProducts |> newProducts
              return oldProducts
